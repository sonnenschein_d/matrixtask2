package me.just._for.fun.matrix;

/*
Есть матрица 2n-1 x 2n-1, заполненная случайными значениями.
Надо вывести их на экран в ряд, начиная из центра по спирали: влево - вниз - вправо - вверх и т.д.
Пример
Если матрица:
1 2 3
4 5 6
7 8 9
То результат:
5 4 7 8 9 6 3 2 1

Сложность алгоритма - квадратичная, так как каждый элемент матрицы, проходя по спирали,
мы посетим ровно 1 раз, а всего элементов (2n-1)^2.
Решить задачу с меньшей сложностью нельзя, так как каждый элемент необходимо посетить хотя бы 1 раз.
 */
public class Task2 {

    private final int[][] matrix;
    private int n;

    public Task2(int[][] matrix) throws IllegalArgumentException{
        checkMatrixNotNull(matrix);
        checkMatrixNotEmpty(matrix);
        checkMatrixDimensions(matrix);
        checkNIsOdd();
        this.matrix = matrix;
    }

    private void checkMatrixNotNull(int[][] matrix) throws IllegalArgumentException{
        if (matrix == null){
            throw new IllegalArgumentException("Matrix is null!");
        }
    }
    private void checkMatrixNotEmpty(int[][] matrix) throws IllegalArgumentException{
        this.n = matrix.length;
        if (n <= 0){
            throw new IllegalArgumentException("Matrix is empty!");
        }
    }
    private void checkMatrixDimensions(int[][] matrix) throws IllegalArgumentException{
        int m = 0;
        for(int i = 0; i < n; i++){
            m = matrix[i].length;
            if (m != n){
                throw new IllegalArgumentException(String.format("Wrong dimensions: expected %d x %d, but was %d x %d", n, n, n, m));
            }
        }
    }
    private void checkNIsOdd() throws IllegalArgumentException{
        if (n % 2 != 1){
            throw new IllegalArgumentException(String.format("Wrong dimensions: expected an odd matrix side length, but got %d", n));
        }
    }

    public String printSpiral(){
        StringBuilder builder = new StringBuilder();

        //center
        int x = n/2;
        int y = n/2;
        builder.append(String.valueOf(matrix[x][y]));
        y--;

        for(int i = 1; i < n/2 + 1; i++){
            //down
            for(int j = 0; j < 2*i; j++, x++){
                builder.append(' ');
                builder.append(String.valueOf(matrix[x][y]));
            }
            x--;
            y++;

            //right
            for(int j = 0; j < 2*i; j++, y++){
                builder.append(' ');
                builder.append(String.valueOf(matrix[x][y]));
            }
            y--;
            x--;

            //up
            for(int j = 0; j < 2*i; j++, x--){
                builder.append(' ');
                builder.append(String.valueOf(matrix[x][y]));
            }
            x++;
            y--;

            //left
            for(int j = 0; j < 2*i; j++, y--){
                builder.append(' ');
                builder.append(String.valueOf(matrix[x][y]));
            }
        }

        return builder.toString();
    }
}
