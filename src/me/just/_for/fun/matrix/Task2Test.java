package me.just._for.fun.matrix;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task2Test {

    @Test(expected = IllegalArgumentException.class)
    public void task2WithNullMatrix() {
        new Task2(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void task2WithEmptyMatrix() {
        new Task2(new int[][]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void task2WithMatrixOfIncorrectDimensions_rowTooShort() {
        new Task2(new int[][]{{1, 2, 3}, {4, 5, 6}, {2, 1}});
    }

    @Test(expected = IllegalArgumentException.class)
    public void task2WithMatrixOfIncorrectDimensions_rowTooLong() {
        new Task2(new int[][]{{1, 2, 3}, {4, 5, 6}, {2, 1, 5, 6}});
    }

    @Test(expected = IllegalArgumentException.class)
    public void task2WithMatrixOfIncorrectDimensions_differentNumberOfRowsAndColumns() {
        new Task2(new int[][]{{1, 2, 3}, {4, 5, 6}});
    }

    @Test(expected = IllegalArgumentException.class)
    public void task2WithEvenMatrix() {
        new Task2(new int[][]{{1, 2}, {3, 4}});
    }

    @Test
    public void printSpiralN1() {
        int[][] matrix = new int[][]{{1}};
        String expected = "1";
        String actual = new Task2(matrix).printSpiral();
        assertEquals(expected, actual);
    }

    @Test
    public void printSpiralN3() {
        int[][] matrix = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        String expected = "5 4 7 8 9 6 3 2 1";
        String actual = new Task2(matrix).printSpiral();
        assertEquals(expected, actual);
    }

    @Test
    public void printSpiralN5() {
        int[][] matrix = new int[][]{{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};
        String expected = "13 12 17 18 19 14 9 8 7 6 11 16 21 22 23 24 25 20 15 10 5 4 3 2 1";
        String actual = new Task2(matrix).printSpiral();
        assertEquals(expected, actual);
    }
}